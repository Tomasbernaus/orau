import React from "react";

import Welcome from './Welcome';
import Portada from './Portada';
import Barra1 from './Nav1';
import Pie from './Pie';

class Principal extends React.Component {
   

    render() {
        let tipoIdioma = "tipo_CA";
        if (this.props.idioma === 1) tipoIdioma = "tipo_ES";
        if (this.props.idioma === 2) tipoIdioma = "tipo_EN";




        return (

            <>
                <Portada />
                <Barra1 claseBarra="barra1" setIdioma={this.props.setIdioma} idioma={this.props.idioma} />




                <Welcome idioma={this.props.idioma} />


                <br />
                <Pie clasepie="pie" idioma={this.props.idioma}/>

            </>




        )
    }
}

export default Principal;