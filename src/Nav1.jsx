import React from 'react';
import { Link } from 'react-router-dom';
import "./Welcome.css";
import IdioCat from './cat.png';
import IdioEsp from './esp.png';
import IdioEng from './eng.png';
import { TEXTOS } from './textos';


class Barra1 extends React.Component {



    render() {

        let clases = "navbar navbar-expand-lg navbar-dark " + this.props.claseBarra;

        return (
            <>
                <nav className={clases}>
                    <div className="container">
                        <p className="m-1 text-white"><Link to="/"><i className="fa fa-chevron-circle-left" aria-hidden="true"></i>
                            {TEXTOS.snavmenu[this.props.idioma]}</Link></p>

                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav ml-auto">
                                <li >
                                    <button className="idio" onClick={() => this.props.setIdioma(0)} ><img className="icon" src={IdioCat} alt="CA" /></button>
                                </li>
                                <li >
                                    <button className="idio" onClick={() => this.props.setIdioma(1)} ><img className="icon" src={IdioEsp} alt="ES" /></button>

                                </li>
                                <li > <button className="idio" onClick={() => this.props.setIdioma(2)} ><img className="icon" src={IdioEng} alt="EN" /></button>

                                </li>


                            </ul>
                        </div>
                    </div>
                </nav>

            </>
        )
    }
}

export default Barra1;
