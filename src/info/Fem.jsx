import React from "react";
import { Link } from 'react-router-dom';
import "./info.css";
import Barra1 from '../Nav1';
import Portada2 from '../Portada2';
import Pie from '../Pie';
import { TEXTOS } from '../textos';


class QuiFem extends React.Component {

    render() {

        return (

            <>
                <Portada2 />
                <Barra1 claseBarra="barra3" setIdioma={this.props.setIdioma} idioma={this.props.idioma} />


                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="lateral">
                                <div class="list-group">
                                <button type="button" class="list-group-item list-group-item-action categ"><Link to="/Info"><h3> {TEXTOS.sinfocat1[this.props.idioma]}</h3></Link>
                                       
                                       </button>
                                       <button type="button" class="list-group-item list-group-item-action categ"><Link to="/Integrants"><h3> {TEXTOS.sinfocat2[this.props.idioma]}</h3></Link>
                                       </button>
                                        <button type="button" class="list-group-item list-group-item-action categ"><Link to="/PerqueFem"><h3> {TEXTOS.sinfocat3[this.props.idioma]}</h3></Link></button>
                                        <button type="button" class="list-group-item list-group-item-action categ"><Link to="/On"><h3>{TEXTOS.sinfocat4[this.props.idioma]}</h3></Link></button>
                                    
                                </div>
                            </div>
                        </div>

                        <div className=" col-md-8">
                            
                            <div className="content">
                            <h1><u>{TEXTOS.sinfocat3[this.props.idioma]}</u></h1>
                            <br/>
                            <p> {TEXTOS.sfem[this.props.idioma]}</p>
                           
                            </div>

                        </div>



                    </div>
                </div>
                <Pie clasepie="pie3" idioma={this.props.idioma} />


            </>



        )

    }
}


export default QuiFem;