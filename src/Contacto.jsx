import React from "react";
import "./App.css";
import {Link} from "react-router-dom";
import Barra1 from './Nav1';
import Portada2 from './Portada2';
import Pie from './Pie';
import { TEXTOS } from './textos';
import ContactUs from './contacto/ContactUs';



class Contacto extends React.Component{

    render(){

        return(

            <>
            <Portada2 />
            <Barra1 claseBarra="barra5" setIdioma={this.props.setIdioma} idioma={this.props.idioma} />
            <br/>
                <div className="row">
                          <div className="col-lg-1 col-xl-1"></div>
        <div className="col-lg-10 col-xl-10">
            <ContactUs idioma={this.props.idioma} />
      

      </div>
     
        <div className="col-lg-1 col-xl-1"></div>
    </div>
                <Pie clasepie="pie5" idioma={this.props.idioma} />
          


            </>



        )

    }
}


export default Contacto;