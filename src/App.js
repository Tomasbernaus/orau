import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Link, Switch, Route} from "react-router-dom";
//import {Container, Row, Col} from 'reactstrap';

import "./App.css";
import { TEXTOS } from "./textos";
import Principal from './Principal';
import Tea from './tea/tea';
import Juega from './tea/teaJuego';
import Busca from './tea/teaBusca';
import Info from './info/Info';
import Empresas from './empresas/empresas';
import Integrants from "./info/Integrants";
import QuiFem from "./info/Fem";
import Donde from "./info/On";
import Formulario from "./empresas/Formulario";
import Contacto from './Contacto';





class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
        idioma: 0,
    }
    this.setIdioma=this.setIdioma.bind(this)
}

setIdioma(id){
  this.setState({idioma:id})
}


  render() {
    let tipoIdioma = "tipo_CA";
        if (this.props.idioma === 1) tipoIdioma = "tipo_ES";
        if (this.props.idioma === 2) tipoIdioma = "tipo_EN";


    return (
      <>

        <BrowserRouter>
          
          
     
       
     

          <Switch>
            
          <Route exact path="/" render={(props)=> <Principal setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          <Route exact path="/T.E.A." render={(props)=> <Tea setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          <Route exact path="/Info" render={(props)=> <Info setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          <Route exact path="/Empresas" render={(props)=> <Empresas setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          <Route exact path="/Juego" render={(props)=> <Juega setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />}/>
          <Route exact path="/Buscador" render={(props)=> <Busca setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          <Route exact path="/Integrants" render={(props)=> <Integrants setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          <Route exact path="/Perquefem" render={(props)=> <QuiFem setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          <Route exact path="/On" render={(props)=> <Donde setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          <Route exact path="/Formulario" render={(props)=> <Formulario setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          <Route exact path="/Contacto" render={(props)=> <Contacto setIdioma={this.setIdioma} idioma={this.state.idioma} {...props} />} />
          
         
          
          </Switch>

        
       
        </BrowserRouter>



      </>
    );
  }
}


export default App;
