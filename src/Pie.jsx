import React from 'react';
import { TEXTOS } from './textos';
import { Link } from 'react-router-dom';
import Redes from './Redes';


class Pie extends React.Component {




    render() {


        return (
            <>
                <footer className={this.props.clasepie}>
                        <p>
                            
                                <Link to="/Contacto">{TEXTOS.spie[this.props.idioma]}</Link>
                            
                        </p>

                    <Redes />

                </footer>

            </>


        )
    }
}

export default Pie;