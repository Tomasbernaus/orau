import React from "react";
import "./Welcome.css";
import { Link, BrowserRouter } from 'react-router-dom';
import { TEXTOS } from "./textos";



class Welcome extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            idioma: 0,
        }
    }



    render() {
        let tipoIdioma = "tipo_CA";
        if (this.props.idioma === 1) tipoIdioma = "tipo_ES";
        if (this.props.idioma === 2) tipoIdioma = "tipo_EN";

        
        return (
            <>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <hr/>
                <div className="fondo">
                        

                    <div className="row">
                        <div className="col-lg-4 col-xl-4">

                            <button type="button" className="btn inicio1"><Link to="/T.E.A."><h1>{TEXTOS.sbutton1[this.props.idioma]}</h1></Link></button>
                        </div>
                        <div className="col-lg-4">
                            <button type="button" className="btn inicio2"><Link to="/Info"><h1>{TEXTOS.sbutton2[this.props.idioma]}</h1></Link></button>
                        </div>
                        <div className="col-lg-4">
                            <button type="button" className="btn inicio3"><Link to="/Empresas"><h1>{TEXTOS.sbutton3[this.props.idioma]}</h1></Link></button></div></div>
                </div>
                <hr/>



            </>
        )
    }
}


export default Welcome;