import React from "react";
import "./tea.css";
import { Link } from 'react-router-dom';

import Barra1 from '../Nav1';
import Pie from '../Pie';
import Portada2 from '../Portada2';
import { TEXTOS } from '../textos';




class Tea extends React.Component {





    render() {



        return (

            <>
                <Portada2 />
                <Barra1 claseBarra="barra2" setIdioma={this.props.setIdioma} idioma={this.props.idioma} />
                

                <div className="container">
                    <div className="tea">


                        <div className="row">


                            <div className="col-12">
                                <button type="button" class="btn btn-lg btn-block green"><Link to="/Buscador"><h3><b>{TEXTOS.sbuttombuscar[this.props.idioma]}</b></h3></Link></button>
                            </div>




                        </div>
                        <br />
                        <br />

                        <div className="row">
                            <div className="col-12">
                                <button type="button" class="btn btn-lg btn-block green"><Link to="/Juego"><h3><b>{TEXTOS.sbuttonaprende[this.props.idioma]}</b></h3></Link></button>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="row"><div className="col-12 volver1">
                    <button type="button" className="btn volver1"><Link to="/">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                        <h6>{TEXTOS.steavolver[this.props.idioma]}</h6></Link></button>
                </div></div>
                
                <Pie clasepie="pie21" idioma={this.props.idioma} />







            </>



        )
    }
}



export default Tea;