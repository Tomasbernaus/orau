import React from "react";
import "./tea.css";
import { Link } from 'react-router-dom';

import Juego from './juego';
import Barra1 from '../Nav1';
import Portada2 from '../Portada2';
import Pie from '../Pie';
import { TEXTOS } from '../textos';




class Juega extends React.Component {





    render() {



        return (

            <>
                <Portada2 />
                <Barra1 claseBarra="barra2" setIdioma={this.props.setIdioma} idioma={this.props.idioma} />
                

                <div className="container">
                    <div className="tea">

                        <div className="row">
                            <div className="col-12"><Juego />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-12 volver1">
                        <button type="button" className="btn volver1"><Link to="/T.E.A."><i class="fa fa-arrow-left" aria-hidden="true"></i>
                            <h6>{TEXTOS.steavolver[this.props.idioma]}</h6></Link></button>
                    </div>
                </div>
                
                <Pie clasepie="pie2" idioma={this.props.idioma} />





            </>



        )
    }
}



export default Juega;