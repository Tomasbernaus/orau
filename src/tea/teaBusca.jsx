import React from "react";
import "./tea.css";
import { Link } from 'react-router-dom';

import Barra1 from '../Nav1';
import Pie from '../Pie';
import Search from "./Search";
import Portada2 from '../Portada2';
import { TEXTOS } from '../textos';



class Busca extends React.Component {





    render() {



        return (

            <>
                <Portada2 />
                <Barra1 claseBarra="barra2" setIdioma={this.props.setIdioma} idioma={this.props.idioma} />
                <div className="row"><div className="col-12 volver1">
                    <button type="button" className="btn volver1"><Link to="/T.E.A."><i class="fa fa-arrow-left" aria-hidden="true"></i>
                        <h6>{TEXTOS.steavolver[this.props.idioma]}</h6></Link></button>
                </div></div>

                <div className="container">
                    <div className="tea">
                        <div className="row">
                            <div className="col-12"><Search idioma={this.props.idioma} />
                            </div>
                        </div>
                    </div>
                </div>
                <Pie clasepie="pie2" idioma={this.props.idioma} />





            </>



        )
    }
}



export default Busca;