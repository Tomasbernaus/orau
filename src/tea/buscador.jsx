import React from 'react';
import { TEXTOS } from '../textos';
import {IMG_URL, API_URL, audios} from './utils';

class Buscador extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: '',
            teas: [],
            audio: 0,
            idioma: 0,
            tipoActual: "tipo_CA",
            audioActual: "/ca/audio_CA"
        }
        this.compare = this.compare.bind(this);
        this.updateState = this.updateState.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();
    }

    updateState(e) {
        this.setState({ data: e.target.value });
    }

    cargaDatos() {

        let url = API_URL + `tea_tea?_p=`;

        fetch(url + "0&_size=100", { method: "GET" })
            .then(data => data.json())
            .then(dadesConvertides => {
                this.setState({ teas: [...this.state.teas, ...dadesConvertides] });
            })
            .catch(err => console.log(err));

        fetch(url + "1&_size=100", { method: "GET" })
            .then(data => data.json())
            .then(dadesConvertides => {
                this.setState({ teas: [...this.state.teas, ...dadesConvertides] });
            })
            .catch(err => console.log(err));
    }

    compare(a, b) {
        let tipo = "tipo_CA";
        if (this.props.idioma === 1) tipo = "tipo_ES";
        if (this.props.idioma === 2) tipo = "tipo_EN";

        if (a[tipo] < b[tipo]) {
            return -1;
        }
        if (a[tipo] > b[tipo]) {
            return 1;
        }
        return 0;
    }


    render() {

        if (this.state.data == ''){
            console.log("hola")
        }

        let tipoIdioma = "tipo_CA";
        if (this.props.idioma === 1) tipoIdioma = "tipo_ES";
        if (this.props.idioma === 2) tipoIdioma = "tipo_EN";

        let audioIdioma = "/audios/ca/";
        if (this.props.idioma === 1) audioIdioma = "/audios/es/";
        if (this.props.idioma === 2) audioIdioma = "/audios/en/";

        let audioActual = "audio_CA";
        if (this.props.idioma === 1) audioActual = "audio_ES";
        if (this.props.idioma === 2) audioActual = "audio_EN";


        let listaTeas = this.state.teas.sort(this.compare)
            .filter(el => el[tipoIdioma].toLowerCase().indexOf(this.state.data) > -1).map(el => {
                return (
                    <tr key={el.id}>
                        <td>{el[tipoIdioma]}</td>
                        <td style={{display: this.props.idioma===0 ? "inline-block" : "none"}}><audio preload="none" controls><source src={audios+'/ca/'+el.audio_CA} /></audio></td>
                        <td style={{display: this.props.idioma===1 ? "inline-block" : "none"}}><audio preload="none" controls><source src={audios+'/es/'+el.audio_ES} /></audio></td>
                        <td style={{display: this.props.idioma===2 ? "inline-block" : "none"}}><audio preload="none" controls><source src={audios+'/en/'+el.audio_EN} /></audio></td>
                        <td><img src={IMG_URL + el.pictograma} width="125px" /></td>
                    </tr>
                );
            });


        return (
            <>
                <h1>
                    {TEXTOS.sintroduce[this.props.idioma]}
                </h1>
                <input type="text" value={this.state.data} onChange={this.updateState} />
                <table>
                    {listaTeas}
                </table>
            </>
        );
    }
}
export default Buscador;