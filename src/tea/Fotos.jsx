
import React from 'react';
import {TEXTOS} from '../textos';

class Fotos extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            teas: [],
            id: -1,
            todos: null,
            pictograma: 0
        }

        this.handleInputChange = this.handleInputChange.bind(this);

    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value,
            idioma: 2
        });
    }

    componentDidMount() {

        this.cargaDatos();

    }

    cargaDatos() {

        let url = `http://192.168.1.10:3000/api/tea_tea?_p=`;

        fetch(url + "0&_size=100", { method: "GET" })
            .then(data => data.json())
            .then(dadesConvertides => {
                this.setState({ teas: [...this.state.teas, ...dadesConvertides]  });
            })
            .catch(err => console.log(err));

        fetch(url + "1&_size=100", { method: "GET" })
            .then(data => data.json())
            .then(dadesConvertides => {
                this.setState({ teas: [...this.state.teas, ...dadesConvertides] });
            })
            .catch(err => console.log(err));
    }



    render() {
        if (this.state.teas.length <120) {
            return <h1>{TEXTOS.scarga[this.props.idioma]}</h1>
        }



        let options = this.state.teas.map(el =>
            <option key={el.id} value={el.id}>{el.tipo_ES}</option>);
        let idactual = (this.state.id===-1) ? this.state.teas[0].id : this.state.id;
        let actual = this.state.teas.filter(el => el.id == idactual)[0];
        
        //para que se muestre un solo pictograma con el select
        let todos = this.state.teas.map(el => {
            if (el===undefined) return <h1>NO</h1>;
            //para que se muestre toda la lista de pictogramas
            return (
                <tr key={el.id}>
                    <td>{el.tipo_CA}</td>
                    <td>{el.tipo_ES}</td>
                    <td>{el.tipo_EN}</td>
                    <td><img src={"http://192.168.1.10:8080/tea/" + el.pictograma} width="75px" /></td>

                </tr>
            );
        });

        return (
            <>
                <h1>Pictogramas</h1>
                {/* <select onChange={this.handleInputChange} name="id">
                    {options}
                </select> */}
                <br />
                <img src={"http://192.168.1.10:8080/tea/" + actual.pictograma} width="100px" />
                <p>{actual.tipo_EN}</p>
                <p>{actual.tipo_CA}</p>
                <h4>Lista de todo</h4>
                <table className="table">
                    <tbody>{todos}</tbody>
                </table>
            </>

        )
    }

}


export default Fotos;
