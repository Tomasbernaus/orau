import React from "react";
import "./empresas.css";
import {Link} from "react-router-dom";
import Barra1 from '../Nav1';
import Portada2 from '../Portada2';
import Pie from '../Pie';
import { TEXTOS } from '../textos';
import Formula from '../contacto/Form';



class Formulario extends React.Component{

    render(){

        return(

            <>
            <Portada2 />
            <Barra1 claseBarra="barra4" setIdioma={this.props.setIdioma} idioma={this.props.idioma} />
            <br/>
                <div className="row">
                          <div className="col-lg-1 col-xl-1"></div>
        <div className="col-lg-10 col-xl-10">
            <Formula idioma={this.props.idioma} />
      

      </div>
     
        <div className="col-lg-1 col-xl-1"></div>
    </div>
                <Pie clasepie="pie4" idioma={this.props.idioma} />
          


            </>



        )

    }
}


export default Formulario;