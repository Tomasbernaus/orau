import React from "react";
import "./empresas.css";
import {Link} from "react-router-dom";
import Barra1 from '../Nav1';
import Portada2 from '../Portada2';
import Pie from '../Pie';
import { TEXTOS } from '../textos';



class Empresas extends React.Component{

    render(){

        return(

            <>
            <Portada2 />
            <Barra1 claseBarra="barra4" setIdioma={this.props.setIdioma} idioma={this.props.idioma} />
            <br/>
                <div className="row">
                          <div className="col-lg-1 col-xl-1"></div>
        <div className="col-lg-5 col-xl-5">
      <div className="texto">
          <p><span className="texto1">
              <h2><b> {TEXTOS.sempresa[this.props.idioma]}</b></h2>
              <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam earum excepturi ab tempore molestias
                eligendi, repudiandae iste placeat dolores minima dolor eaque enim fugiat, ipsa minus labore quae
                delectus deserunt?
            
             
              </h4>
            </span></p>
        </div>

      </div>
      <br/>
        <div className=" col-lg-5 col-xl-5">
      <div className="formulario">
      <h2><b> {TEXTOS.sempresa2[this.props.idioma]}</b></h2>
       
        <br/>
        <br/>
        <button type="button" className="btn inicio3"><Link to="/Formulario"><h1>{TEXTOS.sbutton3[this.props.idioma]}</h1></Link></button>
        
          
        </div>

      </div>
        <div className="col-lg-1 col-xl-1"></div>
    </div>
                <Pie clasepie="pie4" idioma={this.props.idioma} />
          


            </>



        )

    }
}


export default Empresas;